const express  =require('express');
const server = express();
const amq =require('amqplib/callback_api');
const port = 3001;


amq.connect('amqp://localhost',(err,con)=>{
    con.createChannel((err,ch)=>{
        let queue = 'FirstQueue';
        ch.assertQueue(queue,{durable:false});
        console.log(`Waiting for message in ${queue}`);

        ch.consume(queue,(message)=>{
                console.log(`received ${message.content}`);
        },{noAck:true});
    });
})

server.listen(port, ()=>{
    console.log(`Application running on port ${port}`);
})