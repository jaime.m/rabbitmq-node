const express  =require('express');
const server = express();
const amq =require('amqplib/callback_api');
const port = 3000;


amq.connect('amqp://localhost',(err,con)=>{
    con.createChannel((err,ch)=>{
        let queue = 'FirstQueue';
        let message ={
            type:'2',
            content:'Hello RabbitMQ'
        };
        ch.assertQueue(queue,{durable:false});
        ch.sendToQueue(queue,Buffer.from(JSON.stringify(message)));
        console.log('The message was send');
    });
    setTimeout(() => {
        con.close();
        process.exit(0);
    }, 500);
})
server.listen(port, ()=>{
    console.log(`Application running on port ${port}`);
})